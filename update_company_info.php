<?php
echo "starting update_company_info";

require( 'includes/type-helpers.php' );
require( 'includes/db-helpers.php' );

setlocale(LC_ALL, 'en_US.UTF8');

echo "starting update_company_info\n\r";

global $wpdb;

$catIDs = $wpdb->get_col( 
    "
    SELECT      id
    FROM        $wpdb->categories

    "
  );


echo "got info:\n\r";
var_dump( $catIDs );

foreach($catIDs as $catID){
    $pagemax = 1;
    $pagenum = 1;

    echo "Updating category info for cat. ID $catID:\n\r";

    for($pagenum=1;$pagenum<=$pagemax;$pagenum++){
      $soapUrl = "http://longisland.simpleviewcrm.com/webapi/listings/soap/listings.cfc?wsdl";
      $soapParameters = Array(
           'username' => "BlackDog_API",
           'password' => "d0ggi3paddle",
           'pagenum'   => $pagenum,
           'pagesize'  => 30,
           'filters' => array(
               'AndOr' => 'Or',
               'filters'   => array(
                  array(
                    "FIELDCATEGORY" => "LISTING",
                    "FieldName" => "SUBCATID",
                    "FilterType" => "EQUAL TO",
                    "FilterValue" => $catID,
                  ),
               ),
           ),    
      );
      $soapClient = new SoapClient($soapUrl, $soapParameters);

      $listingsResponse = $soapClient->__soapCall('getListings', $soapParameters);

      echo "Page $pagenum";

      if($pagenum == 1){
        $pagemax = round_up($listingsResponse['STATUS']['RESULTS']/30);
      }  

      $clients = $listingsResponse['DATA'];
 
      echo "Showing data:\n\r";
      var_dump( $clients );

       if(!empty($clients)){
         foreach($clients as $client){
          $up_it = false;
           $cpid = $client['LISTINGID'];
           //error_log($client['SORTCOMPANY']);
           $member_ids = $member_ids.",".$cpid;
           $ispost = get_posts(array(
            'numberposts' => 1,
            'post_type' => 'businesses',
            'post_status' => 'publish',
            'meta_key' => 'bizid',
            'meta_value' => $cpid
          ));


          if(empty($ispost)){
            //error_log($client['SORTCOMPANY'].' NEW');
            $apost = array(
              'post_title'    => $client['SORTCOMPANY'],
              'post_status'   => 'publish',
              'post_type'     => 'businesses'
            );
            $post_id = wp_insert_post($apost);
            $up_it = true;

          }else{
            //error_log($client['SORTCOMPANY'].' UPDATED');
            $post_id = $ispost[0]->ID;
            $lastUp = get_field('last_updated', $post_id);
            $update = strtotime($client['LASTUPDATED']);

            if(($lastUp < $update) || empty($lastUp)){

              $up_it = true;

            }

          }

          if($up_it){ 

            all_info($cpid,$catID,$post_id);

          }


         }
       }
    }
} 
?>
