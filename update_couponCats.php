<?php
echo "starting update_couponCats";

require( 'includes/type-helpers.php' );

setlocale(LC_ALL, 'en_US.UTF8');

global $wpdb;
$soapUrl = "http://longisland.simpleviewcrm.com/webapi/listings/soap/listings.cfc?wsdl";
$baseUrl = "/simpleviewapi/getCoupListings.php";
$soapParameters = Array(
    'username' => "BlackDog_API",
    'password' => "d0ggi3paddle",
);

$soapClient = new SoapClient($soapUrl, $soapParameters);

$response = $soapClient->__soapCall('getCouponCats', $soapParameters);

$cats = $response['DATA'];

echo "Got coupon data:\n\r";
var_dump( $cats );

foreach($cats as $cat){
    $name = $cat['COUPONCATNAME'];
    $url_name = toAscii($name);
    $id = $cat['COUPONCATID'];

    echo "cat data: $name - $url_name - $id";

    $args = array(
        'name'=> $name,
        'url_name'=> $url_name,
        'id'=> $id
    );
    $wpdb->query( $wpdb->prepare( 
        "
        INSERT INTO wp_coupon_cats
          ( name, url_name, id)
        VALUES
          (%s, %s, %d)
        ON DUPLICATE KEY UPDATE
          name = VALUES(name), url_name = VALUES(url_name)
      ",$args));
    }        


?>
