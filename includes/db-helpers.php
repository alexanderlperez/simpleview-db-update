<?php

function all_info($cpid,$catID,$post_id){
    global $wpdb;

    $soapUrl = "http://longisland.simpleviewcrm.com/webapi/listings/soap/listings.cfc?wsdl";
    $coupCatURL = "/simpleviewapi/getCoupListings.php";
    $soapParameters = Array(
        'username' => "BlackDog_API",
        'password' => "d0ggi3paddle",
        'listingid'=> $cpid,
    );

    $soapClient = new SoapClient($soapUrl, $soapParameters);


    $listingsResponse = $soapClient->__soapCall('getListing', $soapParameters);

    $info = $listingsResponse['DATA'];

    $update = strtotime($info['LASTUPDATED']);
    //cho $update.'<br/>';

    $mypost = array(
        'ID'           => $post_id,
        'post_title'   =>  $info['SORTCOMPANY'],
        'post_content' => $info['DESCRIPTION'],
    );

    wp_update_post( $mypost );

    $args = array(
        'field_55f9a07e6c8c2' => $catID, //Subcategory ID
        'field_55f9a03e6c8be' => $cpid, // ID
        'field_55f9a0336c8bd' => toAscii($info['SORTCOMPANY']), //url_name
        'field_55f9a0e06c8c4' => $info['ADDR1'],//addr1
        'field_55f9a0f86c8c7' => $info['CITY'],//city
        'field_55f9a0ff6c8c8' => $info['STATE'],//state
        'field_55f9a1066c8c9' => $info['ZIP'],//zip
        'field_55f9a1396c8cd' => $info['PHONE'], //phone
        'field_55f9a0556c8c0' => $info['CATNAME'],//catname
        'field_5600b01f02d7d' => $info['LATITUDE'],
        'field_5600b02802d7e' => $info['LONGITUDE'],
        'field_55f9a0466c8bf' => $update //last_update
    );

    $trim = trim($info['ADDR2']);
    if(!empty($trim)){
        $args['field_55f9a0e86c8c5'] = $info['ADDR2']; //addr2
    }

    $trim = trim($info['ADDR3']);
    if(!empty($trim)){
        $args['field_55f9a0f06c8c6'] = $info['ADDR3']; //addr3
    }

    $trim = trim($info['PRIMARYCONTACTFULLNAME']);
    if(!empty($trim)){
        $args['field_55f9a1246c8cc'] = $info['PRIMARYCONTACTFULLNAME']; //primary_contact
    }

    $trim = trim($info['EMAIL']);
    if(!empty($trim)){
        $args['field_55f9a15a6c8ce'] = $info['EMAIL']; //email
    }

    $trim = trim($info['WEBURL']);
    if(!empty($trim)){
        $args['field_55f9a15f6c8cf'] = $info['WEBURL']; //weburl
    }

    $trim = trim($info['REGION']);
    if(!empty($trim)){
        $args['field_55f9a11c6c8cb'] = $info['REGION']; //region
    }

    $trim = trim($info['LOGOFILE']);
    if(!empty($trim)){
        $args['field_55f9a16b6c8d0'] = $info['LOGOFILE']; //logofile
    }

    $trim = trim($info['IMGPATH']);
    if(!empty($trim)){
        $args['field_55f9a1756c8d1'] = $info['IMGPATH']; //imgpath
    }

    $trim = trim($info['PHOTOFILE']);
    if(!empty($trim)){
        $args['field_55f9a17d6c8d2'] = $info['PHOTOFILE']; //photofile
    }

    $trim = trim($info['ADDITIONALACCOUNTINFORMATION'][0]['VALUEARRAY'][0]);
    if(!empty($trim)){
        $args['field_55f9a1146c8ca'] = $trim; //county
    }

    $amenities= array();
    foreach($info['AMENITIES'] as $amenity){
        $am = NULL;
        $trim = trim($amenity['VALUE']);
        if(!empty($trim)){
            $am = str_replace('_',' ',$amenity['NAME']);
            // if(strcmp($trim,'1') == 0 || $trim == 1){
            //   $am = $am.': '.$trim;
            // }      
        }
        if($am != NULL){
            $amenities[] = array('amenity' => $am);
        }

    }

    if(!empty($amenities)){
        $args['field_55f9a1f26c8d9'] = $amenities;
    }

    if(!empty(trim($info['SUBCATNAME']))){
        $args['field_55f9a06d6c8c1'] = $info['SUBCATNAME'];

        // update the additional_subcategories field re: ACF
        // add a postmeta entry for the post

        if(!empty($info['ADDITIONALSUBCATS'])){

            $addsubcats = array();

            $index = 1;
            foreach($info['ADDITIONALSUBCATS'] as $sub){

                $addsubcats[] = array( 'subcategory' => $sub['SUBCATNAME']);

                /*
                 * save a subcategory repeater field entry
                 * the subfields need to refer to the parent field, the row index, and the subfield name as:
                 * 
                 * update_sub_field( array( $parent_field, $index, $subfield_name ), $content, $post_id )
                 */

                $parent_field = 'additional_subcats';
                $subfield_name = 'name';
                $content = empty( $sub['SUBCATNAME'] ) ? 'MISSING' : $sub['SUBCATNAME'];

                error_log( "Saving subcategory data $content as subfield $subfield_name with an index of $index under $parent_field for $post_id" );

                error_log( update_sub_field( array( $parent_field, $index, $subfield_name ), $content, $post_id ) );

                $index++;
            }

            $args['field_55f9a0c26c8c3'] = $addsubcats;
        }
    }

    if(!empty($info['IMAGES'])){
        $images = array();
        $image = array();
        foreach($info['IMAGES'] as $img){

            if(!empty($img['MEDIANAME'])){
                $image['name'] = $img['MEDIANAME'];
            }
            if(!empty($img['MEDIAFILE'])){
                $image['mediafile'] = $img['MEDIAFILE'];
            }
            if(!empty($img['MEDIADESC'])){
                $image['mediadesc'] = $img['MEDIADESC'];
            }
            if(!empty($img['THUMFILE'])){
                $image['thumbfile'] = $img['THUMFILE'];
            }
            if(!empty($img['IMGPATH'])){
                $image['imgpath'] = $img['IMGPATH'];
            }
            if(!empty($img['TYPE'])){
                $image['type'] = $img['TYPE'];
            }
            if(!empty($img['TYPEID'])){
                $image['typeid'] = $img['TYPEID'];
            }
            $images[] = $image;
        }
        $args['field_55f9a1866c8d3'] = $images;
    }

    foreach($args as $key=>$value){
        update_field($key, $value, $post_id);
    }
}
