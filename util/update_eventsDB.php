<?php
      global $wpdb;
      $url = "http://cs.simpleviewinc.com/feeds/events.cfm?apikey=485C6763-5056-A36A-1C61890F98EE9B2E";
      $xml = file_get_contents($url);
      
      $doc = new DOMDocument;
      $doc->documentURI = $url;
      $doc->preserveWhiteSpace = false;
      $doc->loadXML($xml);
      
      $feed = new SimpleXmlElement($doc->saveXML());
      $eventcols = array();

      $table_name = $wpdb->prefix . 'events';

      foreach ( $wpdb->get_col( "DESC " . $table_name, 0 ) as $column_name ) {

        $eventcols[] = $column_name;

      }

      foreach($feed->events->event as $event){
          $info = array();
          foreach($event->children() as $tag){
              

              $trim = trim($tag);
              if($tag->getName() == 'eventcategories'){
                $cats = array();
                foreach($tag->eventcategory as $tag2){
                    $cats[$tag2->categoryid->__toString()] = $tag2->categoryname->__toString();
                }
                $info['eventcategories'] = serialize($cats);

              }elseif($tag->getName() == 'eventdates'){
                $dates = array();
                foreach($tag->eventdate as $tag2){
                    $dates[] = $tag2->__toString();
                }
                $info['eventdates'] = serialize($dates);
          
              }elseif(strlen($trim) > 0 && in_array($tag->getName(), $eventcols)){
                  $info[$tag->getName()] = $tag->__toString();
              }
              

          }
          var_dump($info);
          $infotype = array();
          foreach($info as $col){
            if(is_numeric($col)){
              $infotype[] = '%d';
            }elseif(is_string($col)){
              $infotype[] = '%s';
            }
          }
          //var_dump($infotype);
          $wpdb->replace('wp_events',
              $info,
              $infotype
            );
      }
?>