<?php
setlocale(LC_ALL, 'en_US.UTF8');

/***
 * Find all members without Lat/Long coords. Do not override.
 *
 *
 ***/

global $wpdb;



$addr = $info['ADDR1'] .', '. $info['CITY'] .', '. $info['STATE'] .', '. $info['ZIP'];
$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($addr)."&sensor=false";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$geoloc = json_decode(curl_exec($ch), true);

$lat = $geoloc['results'][0]['geometry']['location']['lat'];
$lng = $geoloc['results'][0]['geometry']['location']['lng'];

// error_log(print_r($lat, true));
// error_log(print_r($lng, true));
