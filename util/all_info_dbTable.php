<?php

require( '../includes/type-helpers.php' );

global $wpdb;
$soapUrl = "http://longisland.simpleviewcrm.com/webapi/listings/soap/listings.cfc?wsdl";
$coupCatURL = "/simpleviewapi/getCoupListings.php";
$soapParameters = Array(
    'username' => "BlackDog_API",
    'password' => "d0ggi3paddle",
    'listingid'=> $cpid,
);

$soapClient = new SoapClient($soapUrl, $soapParameters);

//var_dump($soapClient->__getFunctions());



$listingsResponse = $soapClient->__soapCall('getListing', $soapParameters);

$info = $listingsResponse['DATA'];



$args = array(
    'sub_id' => $catID,
    'id' => $cpid,
    'name' => $info['SORTCOMPANY'],
    'url_name' => toAscii($info['SORTCOMPANY']),
    'description' => $info['DESCRIPTION'],
    'addr1' => $info['ADDR1'],
    'city' => $info['CITY'],
    'state' => $info['STATE'],
    'zip' => $info['ZIP'],
    'phone' => $info['PHONE'],
    'catname' => $info['CATNAME'],
    'last_update' => $info['LASTUPDATED']
);

$trim = trim($info['ADDR2']);
if(!empty($trim)){
    $args['addr2'] = $info['ADDR2'];
}
$trim = trim($info['ADDR3']);
if(!empty($trim)){
    $args['addr3'] = $info['ADDR3'];
}
$trim = trim($info['PRIMARYCONTACTFULLNAME']);
if(!empty($trim)){
    $args['primary_contact'] = $info['PRIMARYCONTACTFULLNAME'];
}
$trim = trim($info['EMAIL']);
if(!empty($trim)){
    $args['email'] = $info['EMAIL'];
}
$trim = trim($info['WEBURL']);
if(!empty($trim)){
    $args['weburl'] = $info['WEBURL'];
}
$trim = trim($info['REGION']);
if(!empty($trim)){
    $args['region'] = $info['REGION'];
}
$trim = trim($info['LOGOFILE']);
if(!empty($trim)){
    $args['logofile'] = $info['LOGOFILE'];
}

$trim = trim($info['IMGPATH']);
if(!empty($trim)){
    $args['imgpath'] = $info['IMGPATH'];
}

$trim = trim($info['PHOTOFILE']);
if(!empty($trim)){
    $args['photofile'] = $info['PHOTOFILE'];
}
$trim = trim($info['ADDITIONALACCOUNTINFORMATION'][0]['VALUEARRAY'][0]);
if(!empty($trim)){
    $args['county'] = $trim;
}
       /* if(!empty(trim($info['SUBCATNAME']))){
            $args['subcatname'] = $info['SUBCATNAME'];

            if(!empty($info['ADDITIONALSUBCATS'])){
                $addsubcats = array();
                foreach($info['ADDITIONALSUBCATS'] as $sub){
                    $addsubcats[] = $sub['SUBCATNAME'];
                }
                $args['additional_subcats'] = serialize($addsubcats);

            }
        }

        if(!empty($info['IMAGES'])){
          $images = array();
          $image = array();
          foreach($info['IMAGES'] as $img){

            if(!empty($img['MEDIANAME'])){
                $image['name'] = $img['MEDIANAME'];
            }
            if(!empty($img['MEDIAFILE'])){
                $image['mediafile'] = $img['MEDIAFILE'];
            }
            if(!empty($img['MEDIADESC'])){
                $image['mediadesc'] = $img['MEDIADESC'];
            }
            if(!empty($img['THUMBFILE'])){
                $image['thumbfile'] = $img['THUMBFILE'];
            }
            if(!empty($img['IMGPATH'])){
                $image['imgpath'] = $img['IMGPATH'];
            }
            if(!empty($img['TYPE'])){
                $image['type'] = $img['TYPE'];
            }
            if(!empty($img['TYPEID'])){
                $image['typeid'] = $img['TYPEID'];
            }
            $images[] = $image;
          }
          $args['images'] = serialize($images);
        }
        $amenities= array();
        foreach($info['AMENITIES'] as $amenity){
          $am = NULL;
          $trim = trim($amenity['VALUE']);
          if(!empty($trim)){
                $am = str_replace('_',' ',$amenity['NAME']);
                if(!(strcmp($trim,"1") || $trim == 1)){
                  $am = $am.': '.$trim;
                }      
          }
          if($am != NULL){
            $amenities[] = $am;
          }

        }
        if(!empty($amenities)){
          $args['amenities'] = serialize($amenities);
       }*/



$ondupl = "";
foreach($args as $key=>$value){
    ${$key} = $value;
    $ondupl = $ondupl.$key." = CASE WHEN VALUES(last_update) > last_update
        THEN VALUES(".$key.")        
        ELSE ".$key."                           
            END 
            ,";
}
$ondupl = rtrim($ondupl,",");

foreach($args as $col){
    if(is_numeric($col)){
        $argtype[] = '%d';
    }elseif(is_string($col)){
        $argtype[] = '%s';
    }
}

$argstr = "(".implode(", ", array_keys($args)).")";
$argtypestr = "(".implode(", ", $argtype).")";
//echo $argstr;
//echo $argtypestr;

//echo '</br>DB request for '.$cpid;
//$wpdb->replace('wp_companyinfo',$args,$argtype);
$wpdb->query( $wpdb->prepare( 
    "
            INSERT INTO wp_companyinfo
              ".$argstr."
            VALUES
              ".$argtypestr."
            ON DUPLICATE KEY UPDATE
              ".$ondupl."
          ",$args));

     /* if(!empty(trim($info['SUBCATNAME']))){
          echo '<p><strong>Subcategory: </strong>'.$info['SUBCATNAME'];

          if(!empty($info['ADDITIONALSUBCATS'])){
              echo '</br> <strong>Additional Subcategories: </strong>';
              foreach($info['ADDITIONALSUBCATS'] as $sub){
                  echo $sub['SUBCATNAME'].', ';
              }
          }
          echo '</p>';
      }

      if(!empty($info['IMAGES'])){
        echo '<h3>Images</h3>';
        foreach($info['IMAGES'] as $img){
          if(!empty($info['MEDIADESC'])){
              echo '<h4>'.$info['MEDIADESC'].'</h4>';
          }
          echo '<img src='.$img['IMGPATH'].' alt="- No Image">';
        }
      }

      echo '<h3>Amenities</h3>';*/
      /*$acount = count($info['Amenities']);
      $colsize = 
      echo '<ul style="columns: 3; -webkit-columns: 3; -moz-columns: 3;">';
      foreach($info['AMENITIES'] as $amenity){
        if(!empty($amenity['VALUE'])){
              echo '<li style="text-transform: capitalize;">'.str_replace('_',' ',$amenity['NAME']);
              if(!strcmp($amenity['VALUE'],'1')){
            echo ': '.$amenity['VALUE'];
              }
        }
        echo '</li>';
      }
      echo "</ul>";*/

?>
