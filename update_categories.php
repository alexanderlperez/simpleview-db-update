<?php
echo "starting update_categories";

require( 'includes/type-helpers.php' );

setlocale(LC_ALL, 'en_US.UTF8');

global $wpdb;

$soapUrl = "http://longisland.simpleviewcrm.com/webapi/listings/soap/listings.cfc?wsdl";
$soapParameters = Array(
    'username' => "BlackDog_API",
    'password' => "d0ggi3paddle",
);
$soapClient = new SoapClient($soapUrl, $soapParameters);

$response = $soapClient->__soapCall('getListingCats', $soapParameters);
$cats = $response['DATA'];

echo "Got listing cat.'s data, updating values:\n\r";
var_dump( $cats );

foreach($cats as $cat) {
    $name = $cat['NAME'];
    $id = $cat['CATID'];

    $subParameters = Array(
        'username' => "BlackDog_API",
        'password' => "d0ggi3paddle",
        'listingcatid' => $id
    );

    $subClient = new SoapClient($soapUrl, $subParameters);

    $subresponse = $soapClient->__soapCall('getListingSubCats', $subParameters);
    $subcats = $subresponse['DATA'];

    echo "Got listing's subcategory data:\n\r";
    var_dump( $subcats );

    foreach($subcats as $subcat) {
        $subname = $subcat['SUBCATNAME'];
        error_log($subcat['SUBCATNAME']);
        $url_subname = toAscii($subname);
        $subID = $subcat['SUBCATID'];
        $args = array(
            'name'=> $subname,
            'url_name'=> $url_subname,
            'id'=> $subID,
            'parent_name'=> $name,
            'parent_id'=> $id
        );
        $wpdb->query( $wpdb->prepare( 
            "
            INSERT INTO wp_categories
              ( name, url_name, id, parent_name, parent_id)
            VALUES
              (%s, %s, %d, %s, %d)
            ON DUPLICATE KEY UPDATE
              name = VALUES(name), url_name = VALUES(url_name), id = VALUES(id)
              , parent_name = VALUES(parent_name), parent_id = VALUES(parent_id), member_ids = member_ids
          ",$args)); 
    }
}
?>
