<?php
echo "starting update_coupons";

require( 'includes/type-helpers.php' );

setlocale(LC_ALL, 'en_US.UTF8');

	global $wpdb;
	
	$catIDs = $wpdb->get_col( 
		"
		SELECT      id
		FROM        {$wpdb->prefix}coupon_cats

		"
	);

    echo "got cat ids:\n\r";
    var_dump( $catIDs );

	foreach($catIDs as $catID){
		$pagemax = 1;
		$pagenum = 1;
		for($pagenum=1;$pagenum<=$pagemax;$pagenum++){
			$soapUrl = "http://longisland.simpleviewcrm.com/webapi/listings/soap/listings.cfc?wsdl";
			$soapParameters = Array(
					'username' => "BlackDog_API",
					'password' => "d0ggi3paddle",
					'pagenum'   => $pagenum,
					'pagesize'  => 30,
					'filters' => array(
							'AndOr' => 'Or',
							'filters'   => array(
									array(
										"FIELDCATEGORY" => "COUPON",
										"FieldName" => "COUPONCATID",
										"FilterType" => "EQUAL TO",
										"FilterValue" => $catID,
										),
								),
					),
			);
			$soapClient = new SoapClient($soapUrl, $soapParameters);

			$listingsResponse = $soapClient->__soapCall('getCoupons', $soapParameters);

            echo "$catID page $pagenum\n\r";
            var_dump( $listingsResponse );


			if($pagenum == 1){
				$pagemax = round_up($listingsResponse['STATUS']['RESULTS']/30);
			}  

			$coups = $listingsResponse['DATA'];
			//print_r($coups);

			$member_ids = "";
			if(!empty($coups)){
				foreach($coups as $coup){
						$up_it = false;
						$cpid = $coup['COUPONID'];
						$member_ids = $member_ids.",".$coup['COUPONID'];
						 $ispost = get_posts(array(
							'numberposts' => 1,
							'post_type' => 'coupons',
							'post_status' => 'publish',
							'meta_key' => 'coupon_id',
							'meta_value' => $cpid
						));
						if(empty($ispost)){
							$post = array(
								'post_title'    => $coup['OFFERTITLE'],
								'post_status'   => 'publish',
								'post_type'     => 'coupons'
							);
							$post_id = wp_insert_post($post);
							$up_it = true;
						}else{
							$post_id = $ispost[0]->ID;
							$lastUp = get_field('last_updated', $post_id);
							$update = strtotime($coup['UPDATEDATE']);

							if(($lastUp < $update) || empty($lastUp)){

								$up_it = true;
								
							}
						 
						}

						if($up_it){ 

							$mypost = array(
									'ID'           => $post_id,
									'post_title'   =>  $coup['OFFERTITLE'],
									'post_content' => $coup['OFFERTEXT'],
							);

							wp_update_post( $mypost );
							error_log($coup['OFFERTITLE']);
							$args = array();
							
							$args['field_55f9a233e3ca4'] = $coup['COUPONID'];
							
							$args['field_55f9a23be3ca5'] = $coup['SORTCOMPANY'];
							$args['field_55f9a242e3ca6'] = $coup['LISTINGID'];
							$args['field_55f9a250e3ca7'] = $coup['PHONE'];

							$trim = trim($coup['PRIMARYCONTACTFULLNAME']);
							if(!empty($trim)){
									$args['field_55f9a27ce3cac'] = $coup['PRIMARYCONTACTFULLNAME'];
							}

							$trim = trim($coup['EMAIL']);
							if(!empty($trim)){
								$args['field_55f9a287e3cad'] = $coup['EMAIL'];
							}
							
							foreach($coup['ADDITIONALINFORMATION'] as $info) {
								
								if($info['NAME'] == 'Rates'){
									$args['field_55f9a275e3cab'] = $info['VALUE'];
								}
								elseif($info['NAME'] == 'Email'){
									$args['field_55f9a28de3cae'] = $info['VALUE'];
								}
								elseif($info['NAME'] == 'Audience Type'){
									$args['field_55f9a255e3ca8'] = $info['VALUE'];
								}
								elseif($info['NAME'] == 'Disclaimer'){
									$args['field_55f9a2a9e3cb0'] = $info['VALUE'];
								}
								elseif($info['NAME'] == 'Instruction'){
									$args['field_55f9a2b1e3cb1'] = $info['VALUE'];
								}

							}

							$args['field_55f9a297e3caf'] = strtotime($coup['UPDATEDATE']);
							
							$customDate = date_format(date_create_from_format('m-d-Y', $coup['POSTSTART']), 'Y-m-d');
							$args['field_55f9a261e3ca9'] = $customDate;

							$customDate = date_format(date_create_from_format('m-d-Y', $coup['POSTEND']), 'Y-m-d');
							$args['field_55f9a26ce3caa'] = $customDate;

							foreach($args as $key=>$value){
								update_field($key, $value, $post_id);
							}
							
					}
				}
			}
			$wpdb->update('wp_coupon_cats',array('member_ids' => trim($member_ids,',')), array('id' => $catID), array('%s'));    
		}
	}

?>
