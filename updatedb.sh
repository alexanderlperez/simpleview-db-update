#!/bin/bash

wp eval-file $1 

if [ $? -eq 0 ] # $? is a special variable in BASH re: exit code of the last command
then
    terminal-notifier -message "Done with $1" && osascript -e "beep 1" 
    mysqldump -u root -p'root' wp_licvb > wp_licvb_after_$1-$(date "+%m-%d-%y").sql
    exit 0
else
    terminal-notifier -message "Error with $1" && osascript -e "beep 1" 
    echo "Error happened at: $(date '+%m-%d-%y')"
    exit 1
fi
