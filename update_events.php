<?php
echo "starting update_events";

setlocale(LC_ALL, 'en_US.UTF8');

$DEBUG_TOGGLE = true;
//$DEBUG_TOGGLE = false;

/*******************
*  TESTING/DEBUG  *
*******************/
if ( $DEBUG_TOGGLE ){
    @ini_set('log_errors', 1);
    @ini_set('display_errors', 0); //[> enable or disable public display of errors (use 'On' or 'Off') <]
    @ini_set('error_log', dirname(__FILE__) . '/wp-content/debug.log'); //[> path to server-writable log file <]
    @ini_set( 'error_reporting', E_ALL ^ E_NOTICE  ); //[> the php parser to  all errors, excreportept notices.  <]
    error_log( "starting update_events");
}
/***/


// get the simpleview feed wholesale

global $wpdb;
$url = "http://cs.simpleviewinc.com/feeds/events.cfm?apikey=485C6763-5056-A36A-1C61890F98EE9B2E";
$xml = file_get_contents( $url );

if ( $DEBUG_TOGGLE ) file_put_contents( 'simpleview-events.xml', $xml );

$doc = new DOMDocument;
$doc->documentURI = $url;
$doc->preserveWhiteSpace = false;
$doc->loadXML( $xml );
$feed = new SimpleXmlElement( $doc->saveXML() );

// - eventcats will be used to generate both to define fields in an actual event
//   as well as to build out the event category data in the DB

$eventcats = array();
$catmems = array();
$count = 0;

// Go through the feed and operate on each event separately

foreach( $feed->events->event as $event ) {

    $up_it = false; // this variable flags whether to update the event being assessed 
    $eventid = $event->eventid->__toString();

    // check that we're operating on an existing post

    $ispost = get_posts( array(
        'numberposts' => 1,
        'post_type' => 'events',
        'post_status' => 'publish',
        'meta_key' => 'event_id',
        'meta_value' => $eventid
    ) );

    // handle the results

    if( empty( $ispost ) ) {

        // create the post if it hasn't already been so

        $post = array(
            'post_title'    => $event->title->__toString(),
            'post_status'   => 'publish',
            'post_type'     => 'events'
        );

        // insert the newly created post,
        // save the new post's id,
        // flag the event post for update

        $post_id = wp_insert_post( $post );
        $up_it = true;

    } else {

        // the event post exists, so:
        // save the existing post's id,
        // and determine whether we need to update this event post's data
        // (determined by comparing the last time the post was updated to the time the Simpleview entry was,
        // or if has never been updated at all)

        $post_id = $ispost[0]->ID;
        $lastUp = get_field('last_updated', $post_id);
        $update = strtotime($event->lastupdated->__toString());

        if( ( $lastUp < $update ) || empty( $lastUp ) ) {
            $up_it = true;
        }

    }


    /*******************
     *  TESTING/DEBUG  *
     *******************/
    if ( $DEBUG_TOGGLE ) {
        // force update our events for testing/debugging
        $up_it = true;
    }
    /***/


    // update it if this event hasn't been updated re: Simpleview's date (or ever)

    if( $up_it ) {

        if ( $DEBUG_TOGGLE ) error_log( "updating events");

        // begin by refreshing the title and content

        $mypost = array(
            'ID'           => $post_id,
            'post_title'   => $event->title->__toString(),
            'post_content' => $event->description->__toString(),
        );
        wp_update_post( $mypost );

        //update the 'last_updated' field for future assessments

        $update = strtotime( $event->lastupdated->__toString() );
        update_field( 'field_55f9a3a60d26f', $update, $post_id );

        // refresh all the event data provided by simpleview,
        // using the $fields hash to cross-reference Simpleview to ACF fields

        $fields = array(
            'eventid' => 'field_55f9a2ff0d259',
            'eventtypeid' => 'field_55f9a30c0d25b',
            'eventtype' => 'field_55f9a3150d25c',
            'startdate' => 'field_55f9a31b0d25d',
            'enddate' => 'field_55f9a3220d25e',
            'recurrence' => 'field_55f9a3280d25f',
            'times' => 'field_55f9a3350d260',
            'location' => 'field_55f9a33a0d261',
            'phone' => 'field_55f9a3060d25a',
            'admission' => 'field_55f9a3460d262', 
            'website' => 'field_55f9a34c0d263',
            'imagefile' => 'field_55f9a3550d264',
            'address' => 'field_55f9a35d0d265',
            'city' => 'field_55f9a3640d266',
            'state' => 'field_55f9a3710d267',
            'zip' => 'field_55f9a3760d268',
            'eventregion' => 'field_55f9a37f0d269',
            'latitude' => 'field_55f9a3850d26a',
            'longitude' => 'field_55f9a38a0d26b',
            'featured' => 'field_55f9a3920d26c',
            'listingid' => 'field_55f9a3990d26d',
            'created' => 'field_55f9a3a00d26e',
        );

        $info = array();

        foreach( $fields as $tag => $field ){
            $value = $event->{$tag}->__toString();
            update_field( $field, $value, $post_id );

            if ( $DEBUG_TOGGLE ) error_log( "updating field $tag with $value" );
        }

        // insert the categories the event belongs to, by field

        $cats = array();
        foreach( $event->eventcategories->eventcategory as $tag2 ){

            $catname = $tag2->categoryname->__toString();
            $catid = $tag2->categoryid->__toString();

            $cats[] = array(
                'category_name' => $catname,
                'category_id' => $catid
            );

            // handle the global arrays that hold event category ids and members
            // this will be used to refresh the DB in wp_event_cats

            if( ! array_key_exists( $catid, $eventcats ) ){
                // if we haven't encountered this category, update the global array that holds the event categories,
                // and insert this current event into the global category members array

                $eventcats[ $catid ] = $catname;
                $catmems[ $catid ] = $eventid;
            } else {
                // the category exists already, so add this event's id to the members list

                $catmems[ $catid ] .= ','.$eventid;
            }
        }
        update_field('field_55f9a3af0d270', $cats, $post_id);

        // update the region_county array

        $locs = array();
        foreach($event->customfields->customfield as $loc){

            $locsname = $loc->name->__toString();
            $locsid = $loc->fieldid->__toString();
            $locsarray = $loc->valuearray;

            foreach($locsarray->arrayitem as $loca){
                $locationName = $loca->__toString();
                $locs[] = array(
                    'region_or_county' => $locationName // sub-field key *must* be configured as this 
                );
            }

        }
        update_field( 'field_56ba2e75d976c', $locs, $post_id );  
        if ( $DEBUG_TOGGLE ) error_log( "resulting locs for post " . $post_id . ": " . print_r( $locs, 1 ) );

        // update the post_meta for region and county to simplify the query process

        $regions = array( 
            'North Fork',
            'South Fork',
            'North Shore',
            'South Shore',
            'Fire Island',
            'Shelter Island',
        );

        $counties = array(
            'Suffolk County',
            'Norfolk County',
        );

        foreach( $locs as $location ) {
            $location = $location[ 'region_or_county' ];

            // test if the entry is a region or county,
            // set the postmeta accordingly

            if ( $DEBUG_TOGGLE ) error_log( "processing current \$location as: " . print_r( $location, 1 ) );

            $has_region = in_array( $location, $regions );
            $has_county = in_array( $location, $counties );

            if ( $has_region ) {
                if ( $DEBUG_TOGGLE ) error_log( 'updating region with: ' . $location );
                update_post_meta( $post_id, 'region', $location );
                continue;
            }

            if ( $has_county ) {
                if ( $DEBUG_TOGGLE ) error_log( 'updating county with: ' . $location );
                update_post_meta( $post_id, 'country', $location ); // NOTE: valid key, not a typo
                continue;
            }
        }

        if ( $DEBUG_TOGGLE ) {
            $region = get_post_meta( $post_id, 'region', true );
            $county = get_post_meta( $post_id, 'country', true ); // NOTE: not a typo, valid key

            if ( $DEBUG_TOGGLE ) error_log( 'Updated event postmeta with id: ' . $post_id . ' region: ' . $region . ' and county: ' . $county );
        }


        /* 
         * NOTE!! MAKE SURE THE FOLLOWING COMMAND HAS BEEN CALLED IN THE DATA TABLE
         * ALTER TABLE wp_events
         * ADD PRIMARY KEY `event_date` (`eventid`, `count`)
         */

        // create the wp_events entries per event date

        $num = 1;
        $dates = array();
        foreach($event->eventdates->eventdate as $tag2){
            $date = $tag2->__toString();
            $customDate = date_format(date_create_from_format('m/d/Y', $date), 'Y-m-d');

            $wpdb->query( $wpdb->prepare( 
                "
                INSERT INTO wp_events
                (eventid, count, post_id, date)
                VALUES
                (%d, %d, %d, %s)
                ON DUPLICATE KEY UPDATE
                eventid = eventid,
                count = count,
                post_id = post_id,
                date = VALUES(date)
                "
            , $eventid, $num, $post_id, $customDate ) );

            $num++;
        }
    }
}

// update the wp_event_cats table with all the event category data 
// compiled throughout the Simpleview events processing

foreach( $eventcats as $id => $name ){
    $wpdb->query( $wpdb->prepare(  
        "
        INSERT INTO wp_event_cats (id, name, member_ids)
        VALUES (%d, %s, %s)
        ON DUPLICATE KEY UPDATE
        id = id,
        name = VALUES(name),
        member_ids = VALUES(member_ids)
        "
    , $id, $name, $catmems[$id] ) );
}
